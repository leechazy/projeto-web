Feature("Teste");

Scenario("Login com sucesso", ({ I }) => {

  I.amOnPage('http://automationpratice.com.br/')
  I.click('Login')
  I.waitForText('Login',10)
  I.fillField('#user','leechazy@gmail.com')
  I.fillField('#password','123456')
  I.click('#btnLogin')
  I.waitForText('Login realizado',3)
})

Scenario("Tentando Logar digitando apenas o email", ({ I }) => {

  I.amOnPage('http://automationpratice.com.br/')
  I.click('Login')
  I.waitForText('Login',10)
  I.fillField('#user','leechazy@gmail.com')
  I.click('#btnLogin')
  I.waitForText('Senha inválida.',3)

}).tag('@sucesso')

Scenario("Tentando logar sem digitar email e senha", ({ I }) => {

  I.amOnPage('http://automationpratice.com.br/')
  I.click('Login')
  I.click('#btnLogin')
  I.waitForText('E-mail inválido.',3)
  
})

Scenario("Tentando logar digitando apenas a senha", ({ I }) => {

  I.amOnPage('http://automationpratice.com.br/')
  I.click('Login')
  I.fillField('#password','123456')
  I.click('#btnLogin')
  I.waitForText('E-mail inválido.',3)

})

